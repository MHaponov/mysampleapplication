[Get started]

To run application please consider:

- running executable file Calculator.exe in /bin/Debug/
- build and run application in Microsoft Visual Studio, or any other simular IDE

[Requirements]

- required .NET Framework 4.7.2
- PULL REQUEST

[License]

Application licensed by Apache License-2.0. This license is free and perfect for open source software.