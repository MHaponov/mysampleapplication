﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            const string availableOperations = "ADDition, SUBtraction, DIVision, MULTIplication";
            Console.WriteLine("Available operations are: " + availableOperations);
            Console.Write("Please enter operation you want to perform: ");
            string operation = Console.ReadLine().ToLower();
            switch(operation)
            {
                case "add":
                case "addition":
                    Addition();
                    break;
                case "sub":
                case "subtraction":
                    Subtraction();
                    break;
                case "div":
                case "division":
                    Division();
                    break;
                case "multi":
                case "multiplication":
                    Multiplication();
                    break;
                default:
                    InvalidOpertion();
                    break;
            }
            Console.ReadKey();
        }

        static void Addition()
        {
            Console.WriteLine("\t*** Addition ***");
            int[] arrayOfNumbers = new int[2];
            arrayOfNumbers = GetTwoNumbers();
            int result = arrayOfNumbers[0] + arrayOfNumbers[1];
            PrintResult("addition", result);
        }

        static void Subtraction()
        {
            Console.WriteLine("\t*** Subtraction ***");
            int[] arrayOfNumbers = new int[2];
            arrayOfNumbers = GetTwoNumbers();
            int result = arrayOfNumbers[0] - arrayOfNumbers[1];
            PrintResult("subtraction", result);
        }

        static void Division()
        {
            Console.WriteLine("\t*** Division ***");
            int[] arrayOfNumbers = new int[2];
            arrayOfNumbers = GetTwoNumbers();
            float result = (float)arrayOfNumbers[0] / arrayOfNumbers[1];
            PrintResult("division", result);
        }

        static void Multiplication()
        {
            Console.WriteLine("\t*** Multiplication ***");
            int[] arrayOfNumbers = new int[2];
            arrayOfNumbers = GetTwoNumbers();
            int result = arrayOfNumbers[0] * arrayOfNumbers[1];
            PrintResult("multiplication", result);
        }

        static int[] GetTwoNumbers()
        {
            int[] arrayOfNumbers = new int[2]; 
            Console.Write("Please enter first number: ");
            arrayOfNumbers[0] = Int32.Parse(Console.ReadLine());
            Console.Write("Please enter second number: ");
            arrayOfNumbers[1] = Int32.Parse(Console.ReadLine());
            return arrayOfNumbers;
        }

        static void PrintResult(string operation, float result)
        {
            Console.WriteLine("Result of " + operation + " is " + result);
        }

        static void InvalidOpertion()
        {
            Console.WriteLine("Invalid operation");
        }
    }
}
